package br.com.elevador;

import javax.swing.*;

public class Main {

    public static void main(String[] args) {
        String capacidadePessoas = JOptionPane.showInputDialog(null, "\nQuantidade de pessoas que cabem no elevador: \n ");
        String totalAndares = JOptionPane.showInputDialog(null, "\nQuantidade de andares do prédio \n ");
        Elevador elevador = new Elevador();
        elevador.incializar(Integer.parseInt(capacidadePessoas), Integer.parseInt(totalAndares));
        int opcao;
        do {
            String opcaoMenu = JOptionPane.showInputDialog(null, "\nAndar atual: "  + elevador.getAndarAtual() + "\nQuantidade de pessoas: " + elevador.getPessoasNoElevador() +  "\n \n1 - Entrar \n2 - Sair \n3 - Subir \n4 - Descer \n5 - Encerrar \n ");
            opcao = Integer.parseInt(opcaoMenu);
            if (opcao == 1) {
                elevador.entrar();
            } else if (opcao == 2) {
                elevador.sair();
            } else if (opcao == 3) {
                elevador.subir();
            } else if (opcao == 4) {
                elevador.descer();
            }
        } while(opcao != 5);
    }

}
