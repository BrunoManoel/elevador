package br.com.elevador;

import javax.swing.*;

public class Elevador {

    private int andarAtual = 0;
    private int totalAndares;
    private int capacidadePessoas;
    private int pessoasNoElevador = 0;


    public void incializar(int capacidadePessoas, int totalAndares) {
        this.capacidadePessoas = capacidadePessoas;
        this.totalAndares = totalAndares;
    }

    public void entrar() {
        if (this.pessoasNoElevador <= this.capacidadePessoas) {
            this.pessoasNoElevador++;
        } else {
            JOptionPane.showMessageDialog(null, "Elevador já está com a quantidade total de pessoas.");
        }
    }

    public void sair() {
        if (this.pessoasNoElevador > 0) {
            this.pessoasNoElevador--;
        } else {
            JOptionPane.showMessageDialog(null, "Não tem ninguém no elevador");
        }
    }

    public void subir() {
        if (this.andarAtual != this.totalAndares) {
            this.andarAtual++;
        } else {
            JOptionPane.showMessageDialog(null, "Você já está no último andar!");
        }
    }

    public void descer() {
        if (this.andarAtual != 0) {
            this.andarAtual--;
        } else {
            JOptionPane.showMessageDialog(null, "Você já está no primeiro anda!");
        }
    }

    public int getAndarAtual() {
        return andarAtual;
    }

    public void setAndarAtual(int andarAtual) {
        this.andarAtual = andarAtual;
    }

    public int getTotalAndares() {
        return totalAndares;
    }

    public void setTotalAndares(int totalAndares) {
        this.totalAndares = totalAndares;
    }

    public int getCapacidadePessoas() {
        return capacidadePessoas;
    }

    public void setCapacidadePessoas(int capacidadePessoas) {
        this.capacidadePessoas = capacidadePessoas;
    }

    public int getPessoasNoElevador() {
        return pessoasNoElevador;
    }

    public void setPessoasNoElevador(int pessoasNoElevador) {
        this.pessoasNoElevador = pessoasNoElevador;
    }
}
